# Brother Print Node

## Functionality

This module create a connection between your nodejs app and brother label printer.
Tested on Brother QL-810w and QL-720NW only but should work on others.

All you need to do its send the template from the module folder to your printer and set the key to 6.

Execute de function  sendPrintData and pass two parameters on array ['data0', 'data1'] and ['local or net', 'printername or ip']

* local or through network (ip)
* work with brother label printers
* print template based
* only print text

### TODO

* print Qr-Code and Barcode
* print Images

THANKS to <a href="https://github.com/KingWu">KingWu</a> - <a href="https://github.com/KingWu/NodePrinterExample">NodePrinterExample</a>  for local static print.