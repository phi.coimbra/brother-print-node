'use strict';

var printer = require("printer");

class localPrint {

    constructor (data, printer) {
        this._data = data;
        this._printerName = printer;
      }

    send () {

        var _self = this;

        printer.printDirect({
            data: _self._data
            , printer: _self._printerName //'Brother QL-810W' // printer name, if missing then will print to default printer
            , type: 'RAW' // type: RAW, TEXT, PDF, JPEG, .. depends on platform
            , success:function(jobID){
                //console.log("sent to printer with ID: " + jobID);
            }
            , error:function(err){console.log(err);}
        });
    }
}

module.exports = localPrint;