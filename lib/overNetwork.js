'use strict';

var net = require('net');

class overNetwork {

    constructor (data, ip) {
        this._data = data;
        this._ip = ip;
      }

    send () {
        var client = new net.Socket();

        var _self = this;

        client.connect({
                host: _self._ip,
                port: 9100
            }, function () {
                //console.log('connected to ' + _self._ip + ':' + 9100);
                client.write(_self._data, function () {
                    //console.log('Print');
                    client.end();
                });
        });

        client.on('error', function (err) {
            //console.log('Error : ', err);
        });

        client.on('close', function () {
            //console.log('socket closed');
        });

    }
}

module.exports = overNetwork;