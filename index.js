'use strict'

var localPrint = require('./lib/localPrint');
var overNetwork = require('./lib/overNetwork');

var util = require('util');

/**
   * Send print job to `Brother`.
   *
   * @param {(Array[])} fields Each text to be printed on template
   * @param {(Array[])} option Option to print or "local" and "printer name" or "net" and "ip" without port default is 9100
   * @param {Boolean} info Boolean to show printer info and verbose
   */

exports.sendPrintData = function (fields, option, info=false)
{

  if(info){
    // Print Printer List
   // console.log("installed printers:\n"+util.inspect(printer.getPrinters(), {colors:true, depth:10}));
   // console.log("supported job commands:\n"+util.inspect(printer.getSupportedJobCommands(), {colors:true, depth:10}));

    var nameInUTF8 = new Buffer(fields[0], 'utf8');
    console.log(nameInUTF8.toString('utf8'));
  }
  

  var printHeader = new Buffer([
    0x1B, 0x69, 0x61, 0x03,  // p-touch template mode
    0x5E, 0x49, 0x49, // initlaize
    0x5E, 0x54, 0x53, 0x30, 0x30, 0x36, // use template 6
  ]);
  var printCommand = new Buffer([0x5E, 0x46, 0x46]);

  var totallength = printHeader.length + printCommand.length;
  
  var bagData = [];
      bagData.push(printHeader)

  var finalCommand = printHeader

  var buffers = [];
  
  for(var i=0; i< fields.length; i++){
    buffers[i] = []
    
    buffers[i][1] = new Buffer(fields[i], "ascii");

    buffers[i][0] = new Buffer([
      0x5E, 0x4F, 0x4E, 0x54, 0x31, 0x00,
      0x5E, 0x44, 0x49, buffers[i][1].length, 0x00 ])

      totallength += buffers[i][0].length + buffers[i][1].length

      bagData.push(buffers[i][0]);
      bagData.push(buffers[i][1]);
  }

  bagData.push(printCommand);

 var finalCommand = Buffer.concat(bagData, totallength);

 var send;

 if(option[0] == 'local'){
    send = new localPrint(finalCommand, option[1])
    send.send();
 }else{
    send = new overNetwork(finalCommand, option[1])
    send.send(); 
 }

}
